﻿using UnityEngine;
using System.Collections;

public class PickUp : MonoBehaviour {


	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Player") 
		{
			PlayerController player = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerController> ();

			player.pickUpCount += 1;
 
			Destroy (this.gameObject);
		}
	}	
}

