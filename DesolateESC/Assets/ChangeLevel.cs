﻿using UnityEngine;
using System.Collections;

public class ChangeLevel : MonoBehaviour {

	public int pickUpsNeeded;
	public string nextLevel;

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Player") 
		{
			PlayerController player = other.gameObject.GetComponent<PlayerController>();

			if (player.pickUpCount >= pickUpsNeeded) 
			{
				Application.LoadLevel(nextLevel);	
			}
			else
			{
				// text box that says "Lia: I still need the medicine!"
			}
		}
	}	
}
