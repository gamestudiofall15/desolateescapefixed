﻿using UnityEngine;
using System.Collections;

public class SpawnRandomPrefab : MonoBehaviour {

	public GameObject[] prefabs;

	public float SpawnRate; 

	// Use this for initialization
	void Start () {
		InvokeRepeating("NewRandomPrefab" , 0, SpawnRate );
	}
	
	void NewRandomPrefab(){

		int randomIndex = Random.Range(0, prefabs.Length  );

		GameObject prefabToSpawn = prefabs[ randomIndex ]; 


		Instantiate( prefabToSpawn , gameObject.transform.position, Quaternion.identity );
	}
}
