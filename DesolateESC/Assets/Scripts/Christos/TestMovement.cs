﻿using UnityEngine;
using System.Collections;

public class TestMovement : MonoBehaviour {

	public float speed;
	
	// Update is called once per frame
	void Update () {

		if (Input.GetAxis ("Horizontal") != 0) {
			this.transform.Translate (Input.GetAxis ("Horizontal") * speed * Time.deltaTime, 0f, 0f);
		}	
	}
}
