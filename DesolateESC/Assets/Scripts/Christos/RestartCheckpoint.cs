﻿using UnityEngine;
using System.Collections;

public class RestartCheckpoint : MonoBehaviour {

	public string Restart;
	public Vector3 newStartPosition;

	public void RestartCheckPoint (){
		Application.LoadLevel (Restart);
	}

	void Awake () {
		DontDestroyOnLoad (this);
	}

	// Use this for initialization
	void Start () {
	}

	
	// Update is called once per frame
	void Update () {
	
	}
}
