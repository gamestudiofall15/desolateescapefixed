﻿using UnityEngine;
using System.Collections;

public class Checkpoints : MonoBehaviour {

	public Vector3 spawnPosition;
	public Vector3 startPosition;
	// Use this for initialization
	void Start () {

		RestartCheckpoint cM = GameObject.FindGameObjectWithTag ("Checkpoint Manager").GetComponent<RestartCheckpoint>();
		if (cM.Restart == Application.loadedLevelName) {
			startPosition = cM.newStartPosition;
		} else {
			startPosition = this.transform.position; // start position is the one that the player has
		}
		spawnPosition = startPosition; // in the start my spwan position is the same as the start
	
	}
	
}
