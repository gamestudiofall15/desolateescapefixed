﻿using UnityEngine;
using System.Collections;

public class CheckpointInstance : MonoBehaviour {

	bool isUsed;
	// Use this for initialization
	void Start () {
		isUsed = false;


	}
	/*
	void Awake () {
		DontDestroyOnLoad (this.gameObject);
	}*/

	void OnTriggerEnter(Collider Other){

		if(Other.gameObject.tag == "Player" && !isUsed) {
			Checkpoints player = Other.gameObject.GetComponent<Checkpoints>();
			player.spawnPosition = this.transform.position; // change the spawn of the player to this checkpoint position
			isUsed = true;
			Debug.Log ("Blah");
		}
	}
	
	// Update is called once per frame
	void Update () {


	}	
}
