﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Jump: MonoBehaviour {
public Text JumpText;

	// Use this for initialization
	void Start () 
	{
		JumpText.text = "";
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Space)){
			JumpText.text = "";}
	}

	void OnTriggerEnter(Collider Other){
		
		if (Other.gameObject.tag == "Player") {
			JumpText.text = "Press Spacebar to Jump";
		}
	}
}
