﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Crouch: MonoBehaviour {
public Text CrouchText;
	
	// Use this for initialization
	void Start () 
	{
		CrouchText.text = "";
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.S)){
			CrouchText.text= "";}
	}
	
	void OnTriggerEnter(Collider Other){
		
		if (Other.gameObject.tag == "Player") {
			CrouchText.text = "Press S to Crouch";
		}
	}
}
