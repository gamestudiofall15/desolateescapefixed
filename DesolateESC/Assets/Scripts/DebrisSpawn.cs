﻿using UnityEngine;
using System.Collections;

public class DebrisSpawn : MonoBehaviour {

	public GameObject debris;
	public float instantiateRate;


	void Start () {
	
		InvokeRepeating("InstantiateDebris",0,instantiateRate);

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void InstantiateDebris(){

		Instantiate(debris, this.transform.position, Quaternion.identity);
	}
}
