﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class PlayerController : MonoBehaviour {
    private Rigidbody rb;

	public bool steps;
	public AudioSource footsteps;
	public AudioClip footstepSound;
	public GameObject myBody;
	public GameObject runGuy;
	public GameObject jumpGuy;
	public GameObject crouchGuy;
	public GameObject pushGuy;
	public GameObject idleGuy;
	public GameObject crouchCollider;
	public GameObject heroPlaceHolder;
	public int startingPitch = 4;
	public int timeToDecrease = 5;
	//public GameObject jumpCollider;

    public float jumpSpeed;
	public float secondJumpSpeed;
	public float regularSpeed;
	//public float runSpeed;
	public float crouchSpeed;
	/*public float slideSpeed;
	public float slideDuration;
	public float slideReqVelocity;*/


	public float jumpDealey;	
	//public float rayCastDistance;

	//public Transform RayCastOrigin1, RayCastOrigin2;
	public GameObject landingCollider;

	private float speed;
	private float currentVelocity;

    private bool grounded = true;
	private bool secondJump;
	//private bool canSlide;
	private bool isAtWall;


	private int verDirection;
	private int horDirection;
	private int otherPosition;

	public int pickUpCount;
	 
	private Vector3 regularScale;
	private Vector3 crouchScale;
	//private Vector3 slideScale;

	private Vector3 lastPosition;

	private PlayerStates playerState;

	public enum PlayerStates {
		Idle,
		Walk,
		//Run,
		Jump,
		Climb,
		//Slide,
		Crouch,
	};

    void Start () {
		steps = true; 
		playerState = PlayerStates.Idle;
		crouchCollider.GetComponent<BoxCollider> ().enabled = false;
		//jumpCollider.GetComponent<BoxCollider> ().enabled = false;
		//myBody.GetComponent<Animator>().SetTrigger("isIdle");
		idleGuy.gameObject.SetActive (true);
		rb = GetComponent<Rigidbody>();
		speed = regularSpeed;
		horDirection = 1;
		verDirection = 1;
		isAtWall = false;


		regularScale = transform.localScale;
		crouchScale = transform.localScale - new Vector3(0, 3f, 0);
		//slideScale = transform.localScale - new Vector3(-0.3f, 0.3f, 0);
		landingCollider.GetComponent<BoxCollider> ().enabled = false;
		lastPosition = this.transform.position;
		footsteps = GetComponent<AudioSource> ();
		pickUpCount = 0;

	}

    void Update()
    {

<<<<<<< HEAD
		//used to detect collisions
		//Debug.Log (playerState.ToString ());
=======

		//if (footsteps.pitch > 0){
		//	footsteps.pitch -= Time.deltaTime * startingPitch / timeToDecrease;
		//}
		Debug.Log (playerState.ToString ());
>>>>>>> 49d3f1dcbb9af4a0919b4ef499e0a67af9594400

		/*if(grounded == true)
		{

			crouchGuy.gameObject.SetActive (false);
			runGuy.gameObject.SetActive (false);
			idleGuy.gameObject.SetActive (true);
			jumpGuy.gameObject.SetActive (false);
			pushGuy.gameObject.SetActive (false);
			//myBody.GetComponent<Animator>().SetTrigger("isIdle");

		}*/
		if (Input.GetAxisRaw ("Horizontal") > 0) {
			horDirection = 1;
			myBody.gameObject.transform.rotation = Quaternion.Euler(0, 90f, 0f);
		} 
		else if (Input.GetAxisRaw ("Horizontal") < 0) {
			horDirection = -1;
			myBody.gameObject.transform.rotation = Quaternion.Euler(0, 270f, 0f);
		}

		if (Input.GetAxisRaw ("Vertical") >= 0) {
			verDirection = 1;
		} 
		else if (Input.GetAxisRaw ("Vertical") < 0) {
			verDirection = -1;
		}
			
		if (Input.GetKey (KeyCode.RightArrow) || Input.GetKey (KeyCode.LeftArrow)) {
			//MoveAudio ();
			if (steps == true && grounded == true){
			footsteps.Play();
			steps = false;
			}
		} else {
			footsteps.Stop() ;
			steps = true;
		}
		if (grounded == false) {
			footsteps.Stop();
		}




		switch (playerState) {
		case PlayerStates.Idle:
			IdleUpdate ();
			break;
		case PlayerStates.Walk:
			WalkUpdate ();
			break;
		/*case PlayerStates.Run:
			RunUpdate ();
			break;*/
		case PlayerStates.Jump:
			JumpUpdate ();
			break;
		case PlayerStates.Climb:
			ClimbUpdate ();
			break;
		/*case PlayerStates.Slide:
			SlideUpdate ();
			break;*/
		case PlayerStates.Crouch:
			CrouchUpdate ();
			break;
		default:
			break;
		}

		currentVelocity = this.transform.position.x - lastPosition.x;

		lastPosition = this.transform.position;

    }

	//void MoveAudio(){
		//if (steps == true) {
			//footsteps.PlayDelayed(1);
			//steps = false;
		//}
	//}
	
    void OnCollisionEnter(Collision collisionInfo)
    {

		Debug.Log (collisionInfo.gameObject.transform);
		if (collisionInfo.gameObject.tag == "Wall") {
			isAtWall = true;

			if( collisionInfo.gameObject.transform.position.x > this.gameObject.transform.position.x) {
				otherPosition = -1;
			}
			else {
				otherPosition = 1;
			}
		}
    }

	void OnCollisionExit(Collision collisionInfo)
	{
		if (collisionInfo.gameObject.tag == "Wall") {
			isAtWall = false;
			otherPosition = 0;
		}
	}

	private void IdleUpdate() {
		if (Input.GetKeyDown(KeyCode.Space) && grounded == true)
		{
			if(verDirection == 1) {
				Jump (); 

			}
			/*else if (verDirection == -1) {
				Slide ();
			}*/
		}

		/*if (Input.GetKeyDown(KeyCode.LeftShift))
		{
			Run ();
		}*/

		if (Input.GetAxisRaw ("Horizontal") != 0) 
		{
			Walk ();
		}

		if (Input.GetKeyDown(KeyCode.S))
		{
			Crouch ();
		}

		if (Input.GetKeyUp (KeyCode.S)) 
		{
			StopCrouch();
		}
	}

	private void WalkUpdate() {
		if (Input.GetKeyDown(KeyCode.Space)  && grounded == true)
		{
			if(verDirection == 1) {

				Jump (); 

			}
			/*else if (verDirection == -1) {
				Slide ();
			}*/
		}

		if (Input.GetAxisRaw ("Horizontal") != 0) 
		{
			Walk ();
		}

		/*if (Input.GetKeyDown(KeyCode.LeftShift))
		{
			Run ();
		}*/

		if (Input.GetKeyDown(KeyCode.S))
		{
			Crouch ();
		}

		if (Input.GetKeyUp (KeyCode.S)) 
		{
			StopCrouch();
		}

		if (Input.GetAxisRaw ("Horizontal") == 0) 
		{
			playerState = PlayerStates.Idle;
			idleGuy.gameObject.SetActive (true);
			crouchGuy.gameObject.SetActive (false);
			runGuy.gameObject.SetActive (false);
			jumpGuy.gameObject.SetActive (false);
			pushGuy.gameObject.SetActive (false);
			//myBody.GetComponent<Animator>().SetTrigger("isIdle");
		}

	}/*

	private void RunUpdate() {
		if (Input.GetKeyDown(KeyCode.Space)  && grounded == true)
		{
			if(verDirection == 1) {
				Jump (); 
			}
			else if (verDirection == -1) {
				Slide ();
			}
		}	

		if (Input.GetKeyUp(KeyCode.LeftShift))
		{
			StopRun ();
		}

		if (Input.GetAxisRaw ("Horizontal") != 0) 
		{
			Walk ();
		}

	}*/

	private void JumpUpdate() {

		if (Input.GetKeyDown(KeyCode.Space) && secondJump == false)
		{
			SecondJump (); 
		}

		if (Input.GetAxisRaw ("Horizontal") != 0) 
		{
			Walk ();
		}
	}

	private void ClimbUpdate() {

		if (Input.GetAxisRaw ("Horizontal") != 0) 
		{
			Walk ();
		}
	}

	/*private void SlideUpdate() {

	}*/

	private void CrouchUpdate() {

		if (Input.GetAxisRaw ("Horizontal") != 0) 
		{
			Walk ();
		}

		if (Input.GetKeyUp (KeyCode.S)) 
		{
			StopCrouch();
		}
	}

	private void Jump() {
		Debug.Log ("Jump!");
		//transform.Translate (0f, jumpSpeed * Time.deltaTime, 0f);
		rb.AddForce(0, jumpSpeed, 0);
		grounded = false;
		playerState = PlayerStates.Jump;
		secondJump = false;
		Invoke ("ActiveLandingCollider", 0.2f);
		crouchGuy.gameObject.SetActive (false);
		runGuy.gameObject.SetActive (false);
		idleGuy.gameObject.SetActive (false);
		jumpGuy.gameObject.SetActive (true);
		pushGuy.gameObject.SetActive (false);
		//jumpCollider.GetComponent<BoxCollider> ().enabled = true;
		//GetComponent<BoxCollider> ().enabled = false;
		speed = crouchSpeed;
		//myBody.GetComponent<Animator>().SetTrigger ("isJump");


	}

	private void SecondJump() {
		Debug.Log ("SecondJump!");

		if (!isAtWall) {
			rb.AddForce((Input.GetAxis("Horizontal") * secondJumpSpeed/8), secondJumpSpeed, 0);
		} 
		else if (isAtWall && horDirection == otherPosition) {
			rb.AddForce((Input.GetAxis("Horizontal") * secondJumpSpeed/8), secondJumpSpeed, 0);
		}

		secondJump = true;
	}

	private void Walk() {
		//myBody.GetComponent<Animator>().SetTrigger ("isRun");
		crouchGuy.gameObject.SetActive (false);
		runGuy.gameObject.SetActive (true);
		idleGuy.gameObject.SetActive (false);
		jumpGuy.gameObject.SetActive (false);
		pushGuy.gameObject.SetActive (false);
		//jumpCollider.GetComponent<BoxCollider> ().enabled = false;
	
			GetComponent<BoxCollider> ().enabled = true;

		if (playerState == PlayerStates.Crouch) {
			runGuy.gameObject.SetActive (false);
			crouchGuy.gameObject.SetActive (true);
			GetComponent<BoxCollider> ().enabled = false;
		} 
		else 
		{

			GetComponent<BoxCollider> ().enabled = true;
		}
		if (playerState == PlayerStates.Jump) 
		{
			runGuy.gameObject.SetActive (false);
			jumpGuy.gameObject.SetActive (true);
		}
		if (!isAtWall) {
			transform.Translate (speed * Input.GetAxis ("Horizontal") * Time.deltaTime, 0f, 0f);
		} 
		else if (isAtWall && horDirection == otherPosition) {
			transform.Translate (speed * Input.GetAxis ("Horizontal") * Time.deltaTime, 0f, 0f);
		}

		if (horDirection > 0) {
			heroPlaceHolder.gameObject.transform.rotation = Quaternion.Euler(0, 0, 0);

		} else {
			heroPlaceHolder.gameObject.transform.rotation = Quaternion.Euler(0, 180, 0);
		}

		if ((playerState != PlayerStates.Jump) && (playerState != PlayerStates.Crouch)) {
			playerState = PlayerStates.Walk;
		}
	}

	/*private void Run() {
		speed = runSpeed;

		if (!isAtWall) {
			transform.Translate (speed * Input.GetAxis ("Horizontal") * Time.deltaTime, 0f, 0f);
		} 
		else if (isAtWall && horDirection == otherPosition) {
			transform.Translate (speed * Input.GetAxis ("Horizontal") * Time.deltaTime, 0f, 0f);
		}

		playerState = PlayerStates.Run;
	}*/

	/*private void StopRun() {
		speed = regularSpeed;
		playerState = PlayerStates.Walk;
	}*/

	private void Crouch() {

			GetComponent<BoxCollider> ().enabled = false;
			crouchCollider.GetComponent<BoxCollider> ().enabled = true;

		playerState = PlayerStates.Crouch;
		//transform.localScale = crouchScale;

		crouchGuy.gameObject.SetActive (true);
		runGuy.gameObject.SetActive (false);
		idleGuy.gameObject.SetActive (false);
		jumpGuy.gameObject.SetActive (false);
		pushGuy.gameObject.SetActive (false);
		//jumpCollider.GetComponent<BoxCollider> ().enabled = false;
		speed = crouchSpeed;
		Debug.Log ("Crouch Model");
		//myBody.GetComponent<Animator>().SetTrigger ("isCrouch");
		grounded = false;
	}

	private void StopCrouch() {
		//transform.localScale = regularScale;
		GetComponent<BoxCollider> ().enabled = true;
		crouchCollider.GetComponent<BoxCollider> ().enabled = false;
		speed = regularSpeed;
		runGuy.gameObject.SetActive (true);
		crouchGuy.gameObject.SetActive (false);
		//jumpCollider.GetComponent<BoxCollider> ().enabled = false;
		grounded = true;

		if(Input.GetAxisRaw ("Horizontal") != 0) {
			playerState = PlayerStates.Walk;
		}
		else {
			playerState = PlayerStates.Idle;
			idleGuy.gameObject.SetActive (true);
			crouchGuy.gameObject.SetActive (false);
			runGuy.gameObject.SetActive (false);
			jumpGuy.gameObject.SetActive (false);
			pushGuy.gameObject.SetActive (false);
			//jumpCollider.GetComponent<BoxCollider> ().enabled = false;
			//myBody.GetComponent<Animator>().SetTrigger("isIdle");
		}

	}

	/*private void Slide() {
		if (!isAtWall) {
			rb.AddForce (slideSpeed * horDirection, 0, 0);
		} 
		else if (isAtWall && horDirection == otherPosition) {
			rb.AddForce (slideSpeed * horDirection, 0, 0);
		}

		rb.AddForce (slideSpeed * horDirection, 0, 0);
		Debug.Log (slideSpeed * horDirection);
		playerState = PlayerStates.Slide;
		transform.localScale = slideScale;
		Invoke ("StopSlide", slideDuration);
	}*/


	/*private void StopSlide () {
		playerState = PlayerStates.Idle;
		transform.localScale = regularScale;
	}*/

	public void Landing() {
		grounded = true;
		landingCollider.GetComponent<BoxCollider> ().enabled = false;
		footsteps.PlayOneShot(footstepSound);
	
		if (playerState == PlayerStates.Jump) {
			//myBody.GetComponent<Animator>().SetTrigger("isIdle");
			playerState = PlayerStates.Idle;
			idleGuy.gameObject.SetActive (true);
			crouchGuy.gameObject.SetActive (false);
			runGuy.gameObject.SetActive (false);
			jumpGuy.gameObject.SetActive (false);
			pushGuy.gameObject.SetActive (false);
			//jumpCollider.GetComponent<BoxCollider> ().enabled = false;
		}
	}

	public void ActiveLandingCollider() {
		landingCollider.GetComponent<BoxCollider> ().enabled = true;
	}


}