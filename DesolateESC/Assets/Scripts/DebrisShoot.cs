﻿using UnityEngine;
using System.Collections;

public class DebrisShoot: MonoBehaviour {
	
	Rigidbody rb;
	
	// Use this for initialization
	void Start () {
		
		rb = GetComponent<Rigidbody> ();
		rb.velocity = Vector3.right * 5;
		
	}
	
}


